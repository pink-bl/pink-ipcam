#!../../bin/linux-x86_64/sts

## You may have to change sts to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/sts.dbd"
sts_registerRecordDeviceDriver pdbbase

## tests
epicsEnvSet("IOCBL", "PINK")
epicsEnvSet("IOCDEV", "IPCAM")

# Prefix for all records
epicsEnvSet("PREFIX", "$(IOCBL):$(IOCDEV):")
epicsEnvSet("BL", "$(IOCBL)")
epicsEnvSet("DEV", "$(IOCDEV)")
# The port name for the detector
epicsEnvSet("PORT",   "URL1")
# The queue size for all plugins
epicsEnvSet("QSIZE",  "20")
# The maximim image width; used for row profiles in the NDPluginStats plugin
epicsEnvSet("XSIZE",  "1920")
# The maximim image height; used for column profiles in the NDPluginStats plugin
epicsEnvSet("YSIZE",  "1080")
# The maximum number of time series points in the NDPluginStats plugin
epicsEnvSet("NCHANS", "2048")
# The maximum number of frames buffered in the NDPluginCircularBuff plugin
epicsEnvSet("CBUFFS", "50")
# The search path for database files
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(ADCORE)/db")

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "25000000")

# Create a URL driver
# URLDriverConfig(const char *portName, int maxBuffers, size_t maxMemory, int priority, int stackSize)
URLDriverConfig("$(PORT)", 0, 0)

## Load record instances
dbLoadRecords("$(ADURL)/db/URLDriver.template","P=$(PREFIX),R=cam1:,PORT=$(PORT),ADDR=0,TIMEOUT=1")

# Create a standard arrays plugin.
NDStdArraysConfigure("Image1", 3, 0, "$(PORT)", 0)
NDStdArraysConfigure("Image2", 3, 0, "$(PORT)", 0)
NDStdArraysConfigure("Image3", 3, 0, "$(PORT)", 0)
NDStdArraysConfigure("Image4", 3, 0, "$(PORT)", 0)

# This waveform only allows transporting 8-bit images
dbLoadRecords("$(ADCORE)/db/NDStdArrays.template", "P=$(PREFIX),R=image1:,PORT=Image1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int8,FTVL=UCHAR,NELEMENTS=6220800")
dbLoadRecords("$(ADCORE)/db/NDStdArrays.template", "P=$(PREFIX),R=image2:,PORT=Image2,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int8,FTVL=UCHAR,NELEMENTS=6220800")
dbLoadRecords("$(ADCORE)/db/NDStdArrays.template", "P=$(PREFIX),R=image3:,PORT=Image3,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int8,FTVL=UCHAR,NELEMENTS=6220800")
dbLoadRecords("$(ADCORE)/db/NDStdArrays.template", "P=$(PREFIX),R=image4:,PORT=Image4,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int8,FTVL=UCHAR,NELEMENTS=6220800")

## extra records
dbLoadRecords("${TOP}/iocBoot/${IOC}/extra.db","BL=$(BL),DEV=$(DEV), BPM=ipcam")

# Load all other plugins using commonPlugins.cmd
< ${TOP}/iocBoot/${IOC}/commonPlugins.cmd

# autosave settings
set_requestfile_path("$(ADCORE)/ADApp/Db")
set_requestfile_path("$(ADURL)/urlApp/Db")
set_requestfile_path("${TOP}/iocBoot/${IOC}")
set_savefile_path("/home/epics/autosave")
set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")

cd "${TOP}/iocBoot/${IOC}"
iocInit

# autosave
create_monitor_set("auto_settings.req", 30, "P=$(PREFIX),R=cam1:,BL=$(BL),DEV=$(DEV)")

## Start any sequence programs
#seq sncxxx,"user=epics"

## cross on over1:6 follows stat2 centroid
dbpf("$(BL):$(DEV):Over1:6:CenterXLink.DOL", "$(BL):$(DEV):Stats2:CentroidX_RBV CP MS")
dbpf("$(BL):$(DEV):Over1:6:CenterYLink.DOL", "$(BL):$(DEV):Stats2:CentroidY_RBV CP MS")
dbpf("$(BL):$(DEV):TIFF1:FilePath", "/EPICS/Pinkdata/BPM")
dbpf("$(BL):$(DEV):TIFF1:FileTemplate", "%s%s")

